# EJEMPLO 01

# Crear función generadora.
def pares():
    for x in range(0, 102, 2):
        yield x

# Imprimir números generados por la función generadora.
print("Listado de todos los números pares del 1 al 100:")
for par in pares():
    print(par)

# -------------------------------------------------------------

# EJEMPLO 02

# Variable que almacena secuencia iterable generada por la función generadora.
numeros = pares()

# Llamar manualmente a cada elemento de la secuencia obtenida, usando la función next().
while True:
    try:
        par = next(numeros)
        print(par)
    except StopIteration:
        print('Sin más valores en el generador')
        break

# -------------------------------------------------------------

# OTROS EJEMPLOS

# Generador 1
numeros = (numero for numero in range(0, 102))

# Generador 2
pares = (numero for numero in numeros if numero % 2 == 0)

print('Listado de números pares:')
for par in pares:
    print(par)